import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/provider_text.dart';
import 'Text.dart';

class SomeButtons extends StatefulWidget {
  @override
  _SomeButtonsState createState() => _SomeButtonsState();
}

class _SomeButtonsState extends State<SomeButtons> {
  String dropdownValue = '0';

  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context);
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextT(),
          DropdownButton<String>(
            value: dropdownValue,

            items: List<String>.generate(101, (int index) => index.toString())
                .map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Container(margin: EdgeInsets.all(10), child: Text(value),),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
                textProvider.changeText(newValue);
                print(newValue);
              });
            },
            //icon: Icon(Icons.format_list_numbered),
            iconSize: 50,
          ),
        ],
      ),
    );
  }
}

