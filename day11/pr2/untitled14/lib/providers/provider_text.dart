import 'package:flutter/cupertino.dart';

class TextProvider with ChangeNotifier {
  String _TextSome = 'Initial text';


  String get TextSome {
    return _TextSome;
  }


  void changeText (String value) {
    _TextSome = value;
    notifyListeners();
  }
}

