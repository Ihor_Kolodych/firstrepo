import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/provider_text.dart';

class TextT extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    String text = Provider.of<TextProvider>(context).TextSome;
    return Text(text,
      style: TextStyle(
          fontSize: 30
      ),);
  }
}
