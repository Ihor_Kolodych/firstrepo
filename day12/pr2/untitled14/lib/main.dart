import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled14/providers/provider_text.dart';

import 'package:untitled14/widgets/botton_inkwell.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      ),
      body: SomeButtons(),
    );
  }
}
