import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled14/helpers/DialogHelper.dart';
import '../providers/provider_text.dart';

class SomeButtons extends StatefulWidget {
  @override
  _SomeButtonsState createState() => _SomeButtonsState();
}

class _SomeButtonsState extends State<SomeButtons> {
  String dropdownValue = '0';

  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          flex: 6,
          child: Container(
            //height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              //shrinkWrap: true,
              itemCount: textProvider.Sum <= 0 ? 0 : textProvider.Sum,
              itemBuilder: (ctx, index) {
                return Container(
                  height: MediaQuery.of(context).size.height,
                  //width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.all(10),
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(20),
                  color: Colors.blue,
                  child: Text('${index + 1} - Container'),
                );
              },
            ),
          ),
        ),
        Flexible(
          flex: 2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(left: 30, bottom: 60, right: 40),
                height: 40,
                width: 40,
                color: Colors.blue,
                child: InkWell(
                  child: Icon(Icons.add),
                  onTap: () {
                    textProvider.changeCounterList();
                    Scaffold.of(context).hideCurrentSnackBar();
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Add new item ${textProvider.Sum}'),
                        duration: Duration(milliseconds: 1000),
                        action: SnackBarAction(
                          label: 'UNDO',
                          onPressed: () {
                            textProvider.changeDecrimentList();
                          },
                        ),
                      ),
                    );
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 30, bottom: 60, right: 40),
                height: 40,
                width: 40,
                color: Colors.blue,
                child: InkWell(
                  child: Icon(Icons.remove),
                  onTap: () {DialogHelper.show(context);
                  } // () {
                  //   textProvider.changeDecrimentList();
                  //   Scaffold.of(context).hideCurrentSnackBar();
                  //   Scaffold.of(context).showSnackBar(
                  //     SnackBar(
                  //       content: Text('Delete item ${textProvider.Sum+1}'),
                  //       duration: Duration(milliseconds: 1000),
                  //       action: SnackBarAction(
                  //         label: 'UNDO',
                  //         onPressed: () {
                  //           textProvider.changeCounterList();
                  //         },
                  //       ),
                  //     ),
                  //   );
                  // },
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
