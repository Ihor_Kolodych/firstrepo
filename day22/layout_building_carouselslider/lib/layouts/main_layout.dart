import 'package:flutter/material.dart';
import 'package:layout_building_carouselslider/layouts/custom_bottombar.dart';
import 'custom_appbar.dart';

class MainLayout extends StatelessWidget {
  final Widget child;

  const MainLayout({this.child});

  @override
  Widget build(BuildContext context) {
    final double circleSizeWidth = MediaQuery.of(context).size.width;
    final double circleSizeHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Color.fromRGBO(244, 234, 221, 1),
        body: Stack(
          children: [
          Positioned(
            left: circleSizeWidth * 0.55,
            top: circleSizeWidth * -0.15,
            child: Container(
              width: circleSizeWidth * 0.75,
              height: circleSizeHeight * 0.45,
              decoration: BoxDecoration(
                color: Color.fromRGBO(233, 93, 85, 1),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            left: circleSizeWidth * 0.45,
            top: circleSizeWidth * -0.02,
            child: Container(
              width: circleSizeWidth * 0.5,
              height: circleSizeHeight * 0.5,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color.fromRGBO(233, 93, 85, 1),
                  width: 1.2,
                ),
              ),
            ),
          ),
          Positioned(
            left: circleSizeWidth * 0.87,
            bottom: circleSizeWidth * 0.85,
            child: Container(
              width: circleSizeWidth * 0.25,
              height: circleSizeHeight * 0.45,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color.fromRGBO(244, 242, 230, 1),
                  width: 1.3,
                ),
              ),
            ),
          ),
          Positioned(
            right: circleSizeWidth * 0.69,
            bottom: circleSizeWidth * -0.30,
            child: Container(
              width: circleSizeWidth * 0.45,
              height: circleSizeHeight * 0.45,
              decoration: BoxDecoration(
                color: Color.fromRGBO(233, 93, 85, 1),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            right: circleSizeWidth * 0.53,
            bottom: circleSizeWidth * -0.50,
            child: Container(
              width: circleSizeWidth * 0.5,
              height: circleSizeHeight * 0.5,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color.fromRGBO(233, 93, 85, 1),
                  width: 1.2,
                ),
              ),
            ),
          ),
          Positioned(
            right: circleSizeWidth * 0.87,
            bottom: circleSizeWidth * -0.1,
            child: Container(
              width: circleSizeWidth * 0.25,
              height: circleSizeHeight * 0.45,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color.fromRGBO(244, 242, 230, 1),
                  width: 1.5,
                ),
              ),
            ),
          ),
          Positioned(
            child: Container(
              width: 12.0,
              margin: EdgeInsets.symmetric(horizontal: 25.0,vertical: 30.0
              ),
              child: CustomAppbar(),
            ),
          ),
            Positioned(
              child: Container(
                height: 120.0,
                margin: EdgeInsets.symmetric(horizontal: 25.0,vertical: 30.0
                ),
                child: CustomAppbar(),
              ),
            ),
            Positioned(
              child: Container(
                height: 120.0,
                margin: EdgeInsets.symmetric(horizontal: 25.0,vertical: 30.0
                ),
                child: CustomAppbar(),
              ),
            ),
            child,
        ],),
    );
  }
}