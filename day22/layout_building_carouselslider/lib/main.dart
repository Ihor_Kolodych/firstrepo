import 'package:flutter/material.dart';
import 'package:layout_building_carouselslider/layouts/custom_bottombar.dart';

import './layouts/main_layout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 130,
                child: CustomBottomBar(),
              )),
        ],
      ),
    );
  }
}
