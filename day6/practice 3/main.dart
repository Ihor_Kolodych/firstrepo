import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime _selectedDate;

  Widget build(BuildContext context) {
    final isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;
    if (isLandscape) {
      print("Orientation  landscape");
    } else {
      print("Orientation  portrait");
    }
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: isLandscape
            ? Container(
                alignment: Alignment.center,
                child: Text(
                  "Orientation  landscape",
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
                color: Colors.red,
                height: MediaQuery.of(context).size.height * 0.75,
                width: MediaQuery.of(context).size.width * 0.75,
              )
            : Text(
                "Orientation  portrait",
                style: TextStyle(color: Colors.black, fontSize: 30),
              ),
      ),
    );
  }
}
