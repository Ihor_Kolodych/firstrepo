import 'package:flutter/material.dart';
import 'package:untitled16/widgets/custom_inkwell.dart';

import '../genetarateRoute.dart';

class DrawerLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Material(child: Text('All page')),
            CustomButton(
              onTap: () {
                Navigator.of(context).pushReplacementNamed(RouteGenerate.ROUTE_FIRST, arguments: 'First Page');
              },
              child: Text('First Page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushReplacementNamed(RouteGenerate.ROUTE_SECOND, arguments: 'Second Page');
              },
              child: Text('Second Page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushReplacementNamed(RouteGenerate.ROUTE_THIRD, arguments: 'Third Page');
              },
              child: Text('Third Page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushReplacementNamed(RouteGenerate.ROUTE_FOURTH, arguments: 'Fourth Page');
              },
              child: Text('Fourth Page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushReplacementNamed(RouteGenerate.ROUTE_MAIN, arguments: 'Main');
              },
              child: Text('Main'),
            ),
          ],
        ),
      ),
    );
  }
}
