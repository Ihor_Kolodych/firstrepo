import 'package:flutter/material.dart';

import 'appbar_layout.dart';
import 'drawer_layout.dart';

class MainLayout extends StatelessWidget {
  final Drawer drawer;

  const MainLayout({this.drawer});

  @override
  Widget build(BuildContext context) {
    final double circleSizeWidth = MediaQuery.of(context).size.width;
    final double circleSizeHeight = MediaQuery.of(context).size.height;
    final GlobalKey _scaffoldKey = new GlobalKey();
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      backgroundColor: Color.fromRGBO(20, 234, 221, 1),
      appBar: AppBarLayout(),
      body: Container(),
      drawer: DrawerLayout(),
    );
  }
}
