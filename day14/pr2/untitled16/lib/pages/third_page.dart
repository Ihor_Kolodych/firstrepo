import 'package:flutter/material.dart';
import 'package:untitled16/layouts/appbar_layout.dart';
import 'package:untitled16/layouts/drawer_layout.dart';

class ThirdPage extends StatelessWidget {
  final String argument;

  const ThirdPage(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      backgroundColor: Colors.deepOrangeAccent,
      body: Text('Third Page'),
      appBar: AppBarLayout(),
      drawer: DrawerLayout(),
    );
  }
}
