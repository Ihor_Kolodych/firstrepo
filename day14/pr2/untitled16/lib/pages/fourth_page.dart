import 'package:flutter/material.dart';
import 'package:untitled16/layouts/appbar_layout.dart';
import 'package:untitled16/layouts/drawer_layout.dart';

class FourthPage extends StatelessWidget {
  final String argument;

  const FourthPage(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      backgroundColor: Colors.grey,
      body: Text('Fourth Page'),
      appBar: AppBarLayout(),
      drawer: DrawerLayout(),
    );
  }
}
