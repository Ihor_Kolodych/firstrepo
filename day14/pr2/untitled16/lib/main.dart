import 'package:flutter/material.dart';

import './layouts/main_layout.dart';
import './genetarateRoute.dart';

void main() {
  runApp(MaterialApp(onGenerateRoute: RouteGenerate.generateRoute),);
}

class MyHomePage extends StatelessWidget {
  final String argument;
  MyHomePage(this.argument);

  @override
  Widget build(BuildContext context) {
    return MainLayout();
  }
}
