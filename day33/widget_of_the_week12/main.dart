import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
List<Color> colors = [Colors.black, Colors.white, Colors.yellowAccent, Colors.blue];
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageView(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Placeholder(
                  fallbackHeight: 100,
                ),
                FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                      'FittedBoxFittedBoxFittedBoxFittedBoxFittedBoxFittedBoxFittedBoxFittedBox'),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 100),
                  child: Text(
                      'ConstrainedBoxConstrainedBoxConstrainedBoxConstrainedBoxConstrainedBox'),
                ),
                Tooltip(
                  message: 'Tooltip',
                  verticalOffset: 60.0,
                  height: 30.0,
                  child: FloatingActionButton(
                    onPressed: _incrementCounter,
                    child: Icon(Icons.add),
                  ),
                ),
              ],
            ),
          ),
          ListView(children: [
            for (int i = 0; i < 4; i++)
              LimitedBox(
                maxHeight: 200,
                child: Container(
                  color: colors[i],
                ),
              )
          ],)
        ],
      ),
    );
  }
}
