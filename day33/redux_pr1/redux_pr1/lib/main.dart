import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_pr1/store/application/app_state.dart';
import 'package:redux_pr1/store/settings_page_vm.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {

  Color darkGray = Color(0xFFa9a9a9);
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SettingsPageViewModel>(
      converter: SettingsPageViewModel.fromStore,
      builder: (BuildContext context, SettingsPageViewModel vm) {
        return SafeArea(
          child: Scaffold(
            drawer: Drawer(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Switch(
                    value: vm.getBoolean,
                    onChanged: (_) {
                      vm.changeColor();
                    },
                  ),
                  FloatingActionButton(
                      child: Icon(Icons.refresh),
                      onPressed: vm.resetCounter)
                ],
              ),
            ),
            appBar: AppBar(
              backgroundColor: vm.getBoolean == false ? Colors.orange : darkGray,
            ),
            body: Center(
              child: Text(
                vm.counter.toString(),
                style: TextStyle(fontSize: 40),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              child: Icon(
                Icons.add,
              ),
              onPressed: vm.incrementCounter,
            ),
          ),
        );
      },
    );
  }
}