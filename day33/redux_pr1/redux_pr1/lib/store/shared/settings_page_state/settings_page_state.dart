import 'dart:collection';
import 'package:redux_pr1/store/shared/reducer.dart';
import 'package:redux_pr1/store/shared/settings_page_state/settings_page_action.dart';

class SettingsPageState {
  final num counter;
  bool appBarColor = false;

  SettingsPageState({
    this.appBarColor,
    this.counter,
  });

  factory SettingsPageState.initial() {
    return SettingsPageState(
      appBarColor: false,
      counter: 0,
    );
  }

  SettingsPageState copyWith({
    num counter,
    bool appBarColor
  }) {
    return SettingsPageState(
      appBarColor: appBarColor ?? this.appBarColor,
      counter: counter ?? this.counter,
    );
  }

  SettingsPageState reducer(dynamic action) {
    return Reducer<SettingsPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        ResetCounter: (dynamic action) => _resetCounter(),
        ChangeColor: (dynamic action) => _changeColor(),
      }),
    ).updateState(action, this);
  }

  SettingsPageState _incrementCounter() {
    return copyWith(counter: counter + 1);
  }

  SettingsPageState _resetCounter() {
    return copyWith(counter: 0);
  }

  SettingsPageState _changeColor() {
    return copyWith(appBarColor: !appBarColor);
  }
}