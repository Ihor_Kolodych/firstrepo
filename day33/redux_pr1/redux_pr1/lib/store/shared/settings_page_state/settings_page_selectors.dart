import 'package:redux/redux.dart';
import 'package:redux_pr1/store/application/app_state.dart';
import 'package:redux_pr1/store/shared/settings_page_state/settings_page_action.dart';

class SettingsPageSelectors {
  static num getCounterValue(Store<AppState> store) {
    return store.state.settingsPageState.counter;
  }

  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() resetCounter(Store<AppState> store) {
    return () => store.dispatch(ResetCounter());
  }

  static void Function() changeColor(Store<AppState> store) {
    return () => store.dispatch(ChangeColor());
  }

  static bool getBoolean(Store<AppState> store) {
    return store.state.settingsPageState.appBarColor;
  }

}