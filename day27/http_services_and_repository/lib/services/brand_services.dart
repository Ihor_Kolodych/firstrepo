import 'package:flutter/material.dart';

class Brand {
  final String name;
  final String price;
  final String image;
  final String description;

  Brand({
    @required this.name,
    @required this.price,
    @required this.image,
    @required this.description,
  });

  factory Brand.fromJson(Map<String, dynamic> json) {
    return Brand(
      name: json['name'] as String,
      price: json['price'] as String,
      image: json['image_link'] as String,
      description: json['description'] as String,
    );
  }
}
