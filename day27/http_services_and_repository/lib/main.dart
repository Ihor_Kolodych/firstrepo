import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_requests_example/page_info.dart';
import './repositories/brand_repositories.dart';
import './services/brand_services.dart';

Future<List<Brand>> fetchPhotos(http.Client client, String brand) async {
  final response = await client.get(Uri.parse('http://makeup-api.herokuapp.com/api/v1/products.json?brand=$brand'));
  return compute(parsePhotos, response.body);
}

List<Brand> parsePhotos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Brand>((json) => Brand.fromJson(json)).toList();
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class PhotosList extends StatelessWidget {
  final List<Brand> photos;

  PhotosList({Key key, @required this.photos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: photos.length,
        itemBuilder: (context, index) {
          return Hero(
            tag: index,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                    color: Colors.blue,
                    padding: EdgeInsets.all(20),
                    child: Material(
                      child: InkWell(
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) {
                              return PageInfo(
                                index: index,
                                name: photos[index].name,
                                url: photos[index].image,
                                price: photos[index].price,
                                description: photos[index].description,
                              );
                            },
                          ),
                        ),
                        child: Flex(
                          direction: Axis.vertical,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: 100,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(photos[index].image),
                                ),
                              ),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                  color: Colors.yellow,
                                  child: Text(
                                    'Price: ${photos[index].price}\$',
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Text(
                                '${photos[index].name}',
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
              ),
            ),
          );
        },
      ),
    );
  }
}
