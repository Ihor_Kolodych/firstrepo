import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class TheListClass {
  String title;

  TheListClass(this.title);
}

class _MyHomePageState extends State<MyHomePage> {
  List<TheListClass> _theList;

  @override
  void initState() {
    super.initState();
    _theList = List<TheListClass>();

    for (var i = 0; i < 10; i++) {
      _theList.add(TheListClass(i.toString()));
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Scaffold(
        body: PageView(
          children: [
            CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: 160.0,
                  flexibleSpace: const FlexibleSpaceBar(
                    title: Text('SliverAppBar'),
                  ),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return Dismissible(
                        key: new ObjectKey(_theList[index]),
                        child: TweenAnimationBuilder<Color>(
                          tween: ColorTween(begin: Colors.blue[100 * (index % 9)], end: Colors.yellow),
                          duration: const Duration(seconds: 3),
                          builder: (BuildContext _, Color value, Widget __) {
                            return Container(
                              height: 100,
                              alignment: Alignment.center,
                              child: Text('$index'),
                              color: value,
                              //color: Colors.green[100 * (index % 9)],
                            );
                          },
                        ),
                      );
                    },
                    childCount: _theList.length,
                  ),
                ),
                SliverGrid(
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200.0,
                    mainAxisSpacing: 10.0,
                    crossAxisSpacing: 10.0,
                    childAspectRatio: 4.0,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return Dismissible(
                        key: new ObjectKey(_theList[index]),
                        child: TweenAnimationBuilder<Color>(
                          tween: ColorTween(begin: Colors.green[100 * (index % 9)], end: Colors.blue),
                          duration: const Duration(seconds: 3),
                          builder: (BuildContext _, Color value, Widget __) {
                            return Container(
                              alignment: Alignment.center,
                              child: Text('$index'),
                              color: value,
                              //color: Colors.green[100 * (index % 9)],
                            );
                          },
                        ),
                      );
                    },
                    childCount: 10,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
