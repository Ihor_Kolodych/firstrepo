import 'package:flutter/material.dart';
import 'package:untitled7/widgets/custom_inkwell.dart';

import '../genetarateRoute.dart';

class SecondScreen extends StatelessWidget {
  final String argument;

  const SecondScreen(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'First page');
              },
              child: Text('First page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_MAIN, arguments: 'Main');
              },
              child: Text('Main'),
            ),
            CustomButton(
              // Within the `FirstScreen` widget
              onTap: () {
                // Navigate to the second screen using a named route.
                Navigator.pop(context);
              },
              child: Text('Last page'),
            ),
          ],
        ),
      ),
    );
  }
}