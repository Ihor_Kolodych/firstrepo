import 'package:flutter/material.dart';
import 'package:untitled7/widgets/custom_inkwell.dart';

import '../genetarateRoute.dart';

class Error extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: [
            Material(child: Text('404 this page is not created, please select an existing one')),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'First Page');
              },
              child: Text('First Page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND, arguments: 'Second Page');
              },
              child: Text('Second Page'),
            ),
            CustomButton(
              // Within the SecondScreen widget
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_MAIN, arguments: 'Main');
              },
              child: Text('Main'),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Center(child: Text('ERROR PAGE')),
      ),
    );
  }
}
