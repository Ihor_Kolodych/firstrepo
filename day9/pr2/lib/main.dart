import 'package:flutter/material.dart';
import 'package:untitled7/widgets/custom_container.dart';
import 'package:untitled7/pages/second_page.dart';
import 'package:untitled7/widgets/list_button.dart';

import 'genetarateRoute.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    onGenerateRoute: RouteGenerater.generateRoute,
  ));
}

class HomePage extends StatelessWidget {
  final argument;

  const HomePage({this.argument});

  static const _url = [
    {
      'color': Colors.red,
      'url': 'https://sun9-22.userapi.com/s/v1/if1/H7Xnl4D-VUT3dx1UqHOkz6-Bdvp4Uo-hwnR9V9Ax-UuqVOmHtpUjp3w-bzmXL7lH2ChaBjxC'
          '.jpg?size=200x0&quality=96&crop=0,0,960,960&ava=1'
    },
    {
      'color': Colors.blue,
      'url': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU',
    },
    {
      'color': Colors.purple,
      'url': 'https://encrypted-tbn0.gstatic'
          '.com/images?q=tbn:ANd9GcTPzEwiCdIHs9Xly7af8SyHRDGlCjTtV7HLKCL9SbZxK3PQ4EocuKSPJe0QRN8BkqDGFPE&usqp=CAU',
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: [
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (ctx, index) {
                return CustomContainer(_url[index]['color'],_url[index]['url']);
              },
              itemCount: _url.length,
            ),
            Material(child: ListButton()),
          ],
        ),
      ),
      appBar: AppBar(),
    );
  }
}
