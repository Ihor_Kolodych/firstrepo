import 'package:flutter/material.dart';

import '../genetarateRoute.dart';
import 'custom_inkwell.dart';

class ListButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomButton(
          onTap: () {
            Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'Second page');
          },
          child: Text('First pages'),
        ),
        CustomButton(
          onTap: () {
            Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND, arguments: 'First page');
          },
          child: Text('Seconds pages'),
        ),

      ],
    );
  }
}
