import 'package:flutter/material.dart';
import 'package:untitled7/widgets/custom_container.dart';
import 'package:untitled7/pages/second_page.dart';

import 'genetarateRoute.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    onGenerateRoute: RouteGenerater.generateRoute,
  ));
}



class HomePage extends StatelessWidget {
  final argument;

  const HomePage({this.argument});

  static const List<String> _url = [
    'https://sun9-22.userapi.com/s/v1/if1/H7Xnl4D-VUT3dx1UqHOkz6-Bdvp4Uo-hwnR9V9Ax-UuqVOmHtpUjp3w-bzmXL7lH2ChaBjxC'
        '.jpg?size=200x0&quality=96&crop=0,0,960,960&ava=1',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6ZFgy5BAvjhqGr6eWhHulrSs-geyMW8BLk8wURrbUPMSZ1DPZjGhbl0C4wUQg7GnZifc&usqp=CAU',
    'https://encrypted-tbn0.gstatic'
        '.com/images?q=tbn:ANd9GcTPzEwiCdIHs9Xly7af8SyHRDGlCjTtV7HLKCL9SbZxK3PQ4EocuKSPJe0QRN8BkqDGFPE&usqp=CAU',
  ];

  static const List<Color> _availableColors = [Colors.red, Colors.blue, Colors.purple];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            return CustomContainer(_availableColors[index], _url[index]);
          },
          itemCount: _availableColors.length,
        ),
      ),
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (ctx, index) {
                return CustomContainer(_availableColors[index], _url[index]);
              },
              itemCount: _availableColors.length,
            ),
            Column(
              children: [
                ElevatedButton(
                  // Within the `FirstScreen` widget
                  onPressed: () {
                    // Navigate to the second screen using a named route.
                    Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'Second page');
                  },
                  child: Text('First pages'),
                ),
                ElevatedButton(
                  // Within the `FirstScreen` widget
                  onPressed: () {
                    // Navigate to the second screen using a named route.
                    Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND, arguments: 'First page');
                  },
                  child: Text('Seconds pages'),
                ),
                ElevatedButton(
                  // Within the `FirstScreen` widget
                  onPressed: () {
                    // Navigate to the second screen using a named route.
                    Navigator.pop(context);
                  },
                  child: Text('Last page'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
