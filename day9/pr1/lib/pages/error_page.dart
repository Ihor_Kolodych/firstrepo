import 'package:flutter/material.dart';

import '../genetarateRoute.dart';

class Error extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ERROR PAGE'),
      ),
      body: Column(
        children: [
          Material(child: Text('404 this page is not created, please select an existing one')),
          ElevatedButton(
            // Within the SecondScreen widget
            onPressed: () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'First Page');
            },
            child: Text('First Page'),
          ),
          ElevatedButton(
            // Within the SecondScreen widget
            onPressed: () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND, arguments: 'Second Page');
            },
            child: Text('Second Page'),
          ),
          ElevatedButton(
            // Within the SecondScreen widget
            onPressed: () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_MAIN, arguments: 'Main');
            },
            child: Text('Main'),
          ),
        ],
      ),
    );
  }
}
