import 'package:flutter/material.dart';

import '../genetarateRoute.dart';

class SecondScreen extends StatelessWidget {
  final String argument;

  const SecondScreen(this.argument);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              // Within the SecondScreen widget
              onPressed: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'First page');
              },
              child: Text('First page'),
            ),
            ElevatedButton(
              // Within the SecondScreen widget
              onPressed: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_MAIN, arguments: 'Main');
              },
              child: Text('Main'),
            ),
            ElevatedButton(
              // Within the `FirstScreen` widget
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.pop(context);
              },
              child: Text('Last page'),
            ),
          ],
        ),
      ),
    );
  }
}