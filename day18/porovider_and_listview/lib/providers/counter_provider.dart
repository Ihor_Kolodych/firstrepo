import 'dart:math';

import 'package:flutter/cupertino.dart';

class CounterProvider with ChangeNotifier {
  int _counterOne = 0;
  List<int> _listNum = [];
  Random random =  Random();

  List<int> get listNum {
    return _listNum;
  }

  int get counterOne {
    return _counterOne;
  }

  bool asc = true;

  void incrementCounter() {
    _counterOne++;
    _listNum = [...List.generate(_counterOne, (index) => index)];
    notifyListeners();
  }

  void minusCounter() {
    _counterOne--;
    notifyListeners();
  }

  void sortList() {
    if (!asc) {
      _listNum.sort((a,b) => a.compareTo(b));
      asc = true;
    } else {
      _listNum.sort((b,a) => a.compareTo(b));
      asc = false;
    }
    notifyListeners();
  }

  void removeRandom() {
    minusCounter();
    _listNum.removeAt(random.nextInt(_listNum.length));
    notifyListeners();
  }
}
