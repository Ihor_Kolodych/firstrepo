void main (){
  List<int> list = [1, 2, 3, 4, 5, 6 ,7];
  double sum = 0.0;
  for(int i =0; i < list.length; i++){
    sum+=list[i];//0+1+2+3+4+5+6+7
  };
  print(sum);
  sum = 0;
  for(int i in list){
    sum+=i;//0+1+2+3+4+5+6+7
  };
  print(sum);
  sum = 0;
  list.forEach((i) {sum+=i;});//0+1+2+3+4+5+6+7
  print(sum);
  sum = 0;
  int i = 0;
  while(i <= list.length){//0+1+2+3+4+5+6+7
    sum+=i;
    i++;
  }
  print(sum);
  sum = 0;
  int a = 0;
  do{
    sum+=a;
    a++;
  }while (a <= list.length);//0+1+2+3+4+5+6+7
  print(sum);
  var mapUsr = {"name": "Igor", 'Email': 'email@appvesto.com', 'phone': '121525125'};
  mapUsr.forEach((key,value) => print('${key}: ${value}'));
}
