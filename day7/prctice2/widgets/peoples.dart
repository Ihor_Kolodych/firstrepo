import 'package:flutter/material.dart';
import '../models/people.dart';

class Peoples extends StatelessWidget {
  final List<People> people = [
    People(name: 'Ihor', icon: Icon(Icons.add), city: 'Odessa'),
    People(name: 'Ihor2', icon: Icon(Icons.remove), city: 'Odessa'),
    People(name: 'Ihor3', icon: Icon(Icons.add_circle_outline), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
    People(name: 'Ihor', icon: Icon(Icons.add), city: 'Odessa'),
    People(name: 'Ihor2', icon: Icon(Icons.remove), city: 'Odessa'),
    People(name: 'Ihor3', icon: Icon(Icons.add_circle_outline), city: 'Odessa'),
    People(name: 'Ihor4', icon: Icon(Icons.remove_circle), city: 'Odessa'),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: ListView.builder(
        itemCount: people.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.only(top: 10, bottom: 10),
            color: Colors.blue,
            child: ListTile(
              leading: people[index].icon,
              title: Text(
                people[index].name,
              ),
              subtitle: Text(
                people[index].city,
              ),
            ),
          );
        },
      ),
    );
  }
}
