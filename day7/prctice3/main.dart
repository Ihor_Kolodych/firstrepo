import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiplicationTable(),
    );
  }
}

class MultiplicationTable extends StatelessWidget {
  Widget Box(String text, Color color) {
    return SizedBox(
      height: 30,
      width: 30,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(),
          color: color,
        ),
        child: Text(
          text,
        ),
      ),
    );
  }

  Table(int x, int y) {
    List<Widget> rowItems = [];
    List<Widget> columnItems = [];
    for (int i = 0; i <= x; i++) {
      columnItems = [];
      for (int j = 0; j <= x; j++) {
        if (j == 0 && i == 0) {
          columnItems.add(Box('', Colors.green));
        } else if (i == 0) {
          columnItems.add(Box((j * 1).toString(), Colors.green));
        } else if (j == 0) {
          columnItems.add(Box((i * 1).toString(), Colors.green));
        }
        else if (j < i) {
          columnItems.add(Box((i * j).toString(), Colors.white));
        }else if (j == i && i != 0 ) {
          columnItems.add(Box((i * j).toString(), Colors.yellowAccent));
        }
        else {
          columnItems.add(Box((i * j).toString(), Colors.yellow));
        }
      }
      rowItems.add(Column(
        children: [...columnItems],
      ));
    }
    return Row(
      children: [...rowItems],
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      ),
      body: Table(10, 10),
    );
  }
}
