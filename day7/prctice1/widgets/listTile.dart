import 'package:flutter/material.dart';
import 'package:untitled3/widgets/ListTile.dart';

class ListTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      margin: EdgeInsets.only(top: 10, bottom: 10),
      child: ListTile(
        leading: Icon(Icons.add),
        title: SelectableText(
          'It\'s me Ihor',
          showCursor: true,
          cursorColor: Colors.black,
        ),
        trailing: Icon(Icons.remove),
      ),
    );
  }
}
