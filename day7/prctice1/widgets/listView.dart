import 'package:flutter/material.dart';
import 'package:untitled3/widgets/listTile.dart';
import 'sizedBox.dart';

class ListViewWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView(padding: const EdgeInsets.all(8), children: [
      SizedBoxWidget(),
      ListTitleWidget(),
      SizedBoxWidget(),
      ListTitleWidget(),
      SizedBoxWidget(),
      ListTitleWidget(),
      SizedBoxWidget(),
    ]);
  }
}
