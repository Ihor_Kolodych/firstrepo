import 'package:flutter/material.dart';

class SelectableTextWidget extends StatefulWidget {
  @override
  _SelectableTextWidgetState createState() => _SelectableTextWidgetState();
}

class _SelectableTextWidgetState extends State<SelectableTextWidget> {
  String massage = 'Tap to my text and I clear';

  void changeText(){
    setState(() {
      massage = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return SelectableText(
      massage,
      style: TextStyle(color: Colors.white),
      showCursor: true,
      cursorColor: Colors.black,
      onTap: changeText,
    );
  }
}
