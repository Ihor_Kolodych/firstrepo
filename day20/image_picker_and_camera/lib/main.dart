import 'dart:async';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'dart:io';

List<CameraDescription> cameras;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();
  runApp(CameraApp());
}

class CameraApp extends StatefulWidget {
  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {
  CameraController controller;
  String path;

  void takePic() {
    controller.takePicture().then((value) {
      path = value.path;
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.max);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return Container();
    }
    return MaterialApp(
      home: Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Positioned(height: 400, width: 600, child: CameraPreview(controller)),
              path != null
                  ? SizedBox(height: 400.0, width: 600, child: Image.file(File(path)))
                  : Positioned(child: SizedBox(height: 400, width: 600, child: CameraPreview(controller))),
              Positioned(
                  bottom: 10,
                  right: 10,
                  child: FloatingActionButton(onPressed: () {
                    takePic();
                  })),
              Positioned(
                  bottom: 10,
                  left: 10,
                  child: FloatingActionButton(onPressed: () {
                    setState(() {
                      path = null;
                    });
                  })),
            ],
          ),
        ),
      ),
    );
  }
}
