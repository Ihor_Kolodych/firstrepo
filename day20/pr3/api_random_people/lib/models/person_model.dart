import 'package:flutter/material.dart';

class Person {
  final String name;
  final int age;
  final String email;
  final String image;

  const Person({
    @required this.name,
    @required this.age,
    @required this.email,
    @required this.image,
  });

  factory Person.fromJson(Map<String, dynamic> map) {
    return Person(
      name: map['results'].first['name']['first'] + ' ' + map['results'].first['name']['last'],
      age: map['results'].first['dob']['age'],
      email: map['results'].first['email'],
      image: map['results'].first['picture']['large'],
    );
  }
}
