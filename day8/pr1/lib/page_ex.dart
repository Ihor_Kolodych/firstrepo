import 'package:flutter/material.dart';
import './widgets/main_layout.dart';

class MyHomePage extends StatelessWidget {
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('123'),
      ),
      padding: EdgeInsets.all(10.0),
      color: Colors.blue,
      child: Material(
        child: Expanded(
          child: InkWell(
            onTap: () {},
            splashColor: Colors.white,
            child: Text('hello', style: Theme.of(context).textTheme.button),
          ),
        ),
        color: Colors.blueGrey,
      ),
      margin: EdgeInsets.all(100),
    );
  }
}
