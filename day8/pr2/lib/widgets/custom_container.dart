import 'dart:math';

import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
  Color color;
  String image;

  CustomContainer(this.color, this.image);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Transform(
        alignment: Alignment.topRight,
        transform: Matrix4.skewY(0.3)..rotateZ(-pi / 12.0),
        child: Container(
          height: 100,
          width: 100,
          margin: EdgeInsets.all(20.0),
          color: color,
          child: ClipOval(child: Image.network(image)),
        ),
      ),
    );
  }
}
