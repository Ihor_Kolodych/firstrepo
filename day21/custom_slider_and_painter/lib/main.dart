import 'package:custom_slider_and_painter/my_painter.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double percentage = 0.0;
  double initial = 0.0;


  @override

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width * 0.75;
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
               Text(
                 percentage.round().toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 70.0,
                ),
              ),
              GestureDetector(
                onHorizontalDragStart: (DragStartDetails details) {
                  initial = details.globalPosition.dx;
                },
                onHorizontalDragUpdate: (DragUpdateDetails details) {
                  double distance = details.globalPosition.dx - initial;
                  double percentageAddition = distance / 400;

                  print('distance ' + distance.toString());
                  setState(() {
                    percentage = (percentage + percentageAddition).clamp(0.0, MediaQuery.of(context).size.width * 0.75);
                  });
                },
                onHorizontalDragEnd: (DragEndDetails details) {
                  initial = 0.0;
                },
                child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: CustomPaint(
                      painter: CustomSliderPainter(this.percentage),
                    ),
                ),
                ),

            ],
          ),
        ),
      ),
    );
  }
}
