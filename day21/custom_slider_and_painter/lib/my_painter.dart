import 'package:flutter/material.dart';

class CustomSliderPainter extends CustomPainter{
  //double totalWidth = 200.0;
  double percentage;

  CustomSliderPainter(this.percentage);
  @override

    void paint(Canvas canvas, Size size) {
      var paint = Paint()
        ..color = Colors.red
        ..strokeWidth = 5
        ..strokeCap = StrokeCap.round;

      Offset startingPoint = Offset(0, size.height / 2);
      Offset endingPoint = Offset(size.width, size.height / 2);

      canvas.drawLine(startingPoint, endingPoint, paint);
      var circle= Paint()
        ..color = Colors.green;

      Offset starting = Offset(percentage, size.height / 2);
      canvas.drawCircle(starting, 5, circle);
    }


    @override
    bool shouldRepaint(CustomSliderPainter oldDelegate) {
      percentage != oldDelegate.percentage;
      return true;
    }

}