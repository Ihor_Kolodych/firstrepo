import 'dart:collection';

import 'package:redux_counter/store/app/reducer.dart';
import 'package:redux_counter/store/counter_page_state/counter_page_state_action.dart';

class CounterPageStateState {
  final int counter;

  CounterPageStateState({
    this.counter,
  });

  factory CounterPageStateState.initial() {
    return CounterPageStateState(
      counter: 0,
    );
  }

  CounterPageStateState copyWith({
    int counter,
  })
  {
    return CounterPageStateState(
      counter: counter ?? this.counter,
    );
  }
  CounterPageStateState reducer(dynamic action) {
    return Reducer<CounterPageStateState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
      }),
    ).updateState(action, this);
  }
  CounterPageStateState _incrementCounter(){
    return copyWith(counter: this.counter+1);
  }
  CounterPageStateState _decrementCounter(){
    return copyWith(counter: this.counter-1);
  }
}
