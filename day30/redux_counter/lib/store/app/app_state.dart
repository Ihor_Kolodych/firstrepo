import 'package:flutter/foundation.dart';
import 'package:redux_counter/store/counter_page_state/counter_page_state_state.dart';

class AppState {
  final CounterPageStateState counterPageStateState;


  AppState({
    @required this.counterPageStateState,
  });

  factory AppState.initial() {
    return AppState(
      counterPageStateState: CounterPageStateState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterPageStateState: state.counterPageStateState.reducer(action),
    );
  }
}