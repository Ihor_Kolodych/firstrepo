import 'dart:math';

import 'package:flutter/cupertino.dart';

class CounterProvider with ChangeNotifier {
  int _counterOne = 0;
  int _counterTwo = 0;

  int get counterOne {
    return _counterOne;
  }

  int get counterTwo {
    return _counterTwo;
  }


  void incrementCounter() {
    Random().nextBool() ? _counterOne++ : _counterTwo++;
    notifyListeners();
  }

  void decrementCounter() {
    Random().nextBool() ? _counterOne-- : _counterTwo--;
    notifyListeners();
  }
}
