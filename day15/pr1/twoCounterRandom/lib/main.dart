import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/counter_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => CounterProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Selector<CounterProvider, int>(
                  builder: (context, data, _) {
                    return Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        '${data}',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    );
                  },
                  selector: (context, countRandom) => countRandom.counterOne,
                ),
                Selector<CounterProvider, int>(
                  builder: (context, count, _) {
                    return Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        '${count}',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    );
                  },
                  selector: (context, countRandom) => countRandom.counterTwo,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FloatingActionButton(
                  onPressed: (){counterProvider.incrementCounter();},
                  tooltip: 'Increment',
                  child: Icon(Icons.add),
                ),
                FloatingActionButton(
                  onPressed: (){counterProvider.decrementCounter();},
                  tooltip: 'Increment',
                  child: Icon(Icons.remove),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
