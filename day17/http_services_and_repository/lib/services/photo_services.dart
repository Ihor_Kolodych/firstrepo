import 'package:flutter/material.dart';

class Photo {
  //final int albumId;
  final String id;
  final String username;
  final String url;
  //final String thumbnailUrl;

  Photo({
    //@required this.albumId,
    @required this.id,
    @required this.username,
    @required this.url,
    //@required this.thumbnailUrl,
  });

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(
      //albumId: json['albumId'] as int,
      id: json['id'] as String,
      username: json['user']['username'] as String,
      url: json['urls']['full'] as String,
      //thumbnailUrl: json['thumbnailUrl'] as String,
    );
  }
}