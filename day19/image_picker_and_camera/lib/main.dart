import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}
class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File takeImage;

  Future<void> getImage(ImageSource source) async {
    final picker = ImagePicker();
    await picker.getImage(source: source).then((value) {
      if (value.path != null) {
        setState(() {
          takeImage = File(value.path);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ImagePicker'),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Scaffold(
            body: Container(
              height: MediaQuery.of(context).size.height,
              decoration: takeImage != null
                  ? BoxDecoration(
                image: DecorationImage(
                    image: FileImage(takeImage), fit: BoxFit.cover),
              )
                  : BoxDecoration(color: Colors.green),
            ),
          ),
           Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: 50,
                    color: Colors.blue,
                    child: InkWell(
                      onTap: () {
                        getImage(ImageSource.gallery);
                      },
                      child: Text('Gallery picker', textAlign: TextAlign.center,),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: 50,
                    color: Colors.yellowAccent,
                    child: InkWell(
                      onTap: () {
                        getImage(ImageSource.camera);
                      },
                      child: Text('Open the camera', textAlign: TextAlign.center,),
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }
}