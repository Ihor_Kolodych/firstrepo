import 'package:dictionary_practice/dictionary/flutter_dictionary.dart';
import 'package:flutter/cupertino.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLane(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}

