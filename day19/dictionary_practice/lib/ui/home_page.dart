
import 'package:dictionary_practice/dictionary/data/en.dart';
import 'package:dictionary_practice/dictionary/dictionary_classes/genaral_language.dart';
import 'package:dictionary_practice/dictionary/dictionary_classes/setting_page_dictionary.dart';
import 'package:dictionary_practice/widgets/lang_list.dart';
import 'package:dictionary_practice/widgets/text.dart';
import 'package:flutter/material.dart';

import '../dictionary/flutter_dictionary.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    GeneralLanguage language =
        FlutterDictionary.instance.language?.generalLanguage ??
            en.generalLanguage;
    SettingsPageDictionary languageTwo =
        FlutterDictionary.instance.language?.settingsPageDictionary ??
            en.settingsPageDictionary;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBar(
              automaticallyImplyLeading: false,
              title: Column(
                children: [
                  Text(FlutterDictionary.instance.language?.generalLanguage?.text??en.generalLanguage.text),
                  Text(FlutterDictionary.instance.language?.settingsPageDictionary?.textTwo??en.settingsPageDictionary.textTwo),
                ],
              ),
            ),
            InkWell(
              child: LangList(),
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
