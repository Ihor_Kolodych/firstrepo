import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Spacer(
            flex: 20,
          ),
          Stack(
            alignment: AlignmentDirectional.topCenter,
            children: <Widget>[
              Container(
                width: 100,
                height: 100,
                color: Colors.amber,
              ),
              Container(
                width: 75,
                height: 75,
                color: Colors.blue,
              ),
            ],
            overflow: Overflow.visible,
          ),
          Divider(
            height: 100,
            thickness: 20,
          ),
          Spacer(
            flex: 20,
          ),
          Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                right: -50,
                child: Container(
                  width: 50,
                  height: 50,
                  color: Colors.black,
                ),
              ),
              Container(
                width: 25,
                height: 25,
                color: Colors.red,
              ),
            ],
            overflow: Overflow.visible,
          ),
          Spacer(
            flex: 20,
          ),
          Divider(
            height: 100,
            thickness: 20,
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
