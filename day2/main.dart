import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            Container(
              child: Text(
                'SampleText',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              color: Colors.black,
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(20),
              alignment: Alignment.center,
              width: 200,
              height: 100,
            ),
            Container(

              child: Image.network('https://betasignup.ubisoft.com/Content/img/Platforms/pc.png'),
              color: Colors.yellowAccent,
              alignment: Alignment.center,
              width: 200,
              height: 100,
            )
          ],
        ),
      ),
    );
  }
}
