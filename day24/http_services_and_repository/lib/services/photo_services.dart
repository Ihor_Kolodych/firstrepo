import 'package:flutter/material.dart';

class Photo {
  final String name;
  final String language;

  Photo({
    @required this.name,
    @required this.language,
  });

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(
      name: json['show']['name'] as String,
      language: json['show']['language'] as String,
    );
  }
}