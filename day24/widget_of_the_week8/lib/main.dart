import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              InkWell(
                onTap: () => showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: const Text('AlertDialog Title'),
                    content: const Text('AlertDialog description'),
                    actions: [
                      InkWell(
                        onTap: () => Navigator.pop(context, 'Cancel'),
                        child: const Text('Cancel'),
                      ),
                      InkWell(
                        onTap: () => Navigator.pop(context, 'OK'),
                        child: const Text('OK'),
                      ),
                    ],
                  ),
                ),
                child: const Text('Show Dialog'),
              ),
              InkWell(
                onTap: () {
                  showCupertinoModalPopup<void>(
                    context: context,
                    builder: (BuildContext context) => CupertinoActionSheet(
                      title: const Text('Title'),
                      message: const Text('Message'),
                      actions: <CupertinoActionSheetAction>[
                        CupertinoActionSheetAction(
                          child: const Text('Action One'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: const Text('Action Two'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ),
                  );
                },
                child: const Text('CupertinoActionSheet'),
              ),
              Builder(
                builder: (BuildContext context) => InkWell(
                  child: const Text('Show Snackbar'),
                  onTap: () {
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        action: SnackBarAction(
                          label: 'Action',
                          onPressed: () {},
                        ),
                        content: const Text('Awesome SnackBar!'),
                        duration: const Duration(milliseconds: 1500),
                        width: 300.0,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20.0,
                        ),
                        behavior: SnackBarBehavior.floating,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    );
                  },
                ),
              ),
              CircularProgressIndicator(
                backgroundColor: Colors.yellow,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
              CupertinoActivityIndicator(
                radius: 25,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
