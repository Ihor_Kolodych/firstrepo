import 'package:flutter/material.dart';

class AppFonts {
  static TextStyle get textBold {
    return TextStyle(
      fontWeight: FontWeight.bold,
    );
  }
}