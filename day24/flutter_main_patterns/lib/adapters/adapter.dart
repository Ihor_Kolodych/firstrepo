class OldRectangle {
  final int x;
  final int y;
  final int width;
  final int height;

  const OldRectangle(this.x, this.y, this.width, this.height);

  void render() {
    print("Rendering OldRectangle...$x ,$y ,$width ,$height");
  }
}

class Rect {
  OldRectangle _oldRect;

  Rect(int left, int top, int right, int bottom) {
    _oldRect = OldRectangle(left, top, right - left, bottom - top);
  }

  void render() => _oldRect.render();
}