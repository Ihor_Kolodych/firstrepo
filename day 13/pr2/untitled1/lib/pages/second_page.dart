import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled1/providers/counter_provider.dart';
import 'package:untitled1/widgets/custom_inkwell.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context);
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          child: Icon(Icons.arrow_back),
          onTap: (() => Navigator.pushNamed(context, '/')),
        ),
      ),
      body: Builder(
        builder: (ctx) => Container(
          margin: EdgeInsets.all(25),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            InkWellCustom(textProvider.changeDecrimentList, Icon(Icons.remove), 'Minus one ='),
            InkWellCustom(textProvider.changeCounterList, Icon(Icons.add), 'Plus one ='),
          ]),
        ),
      ),
    );
  }
}
