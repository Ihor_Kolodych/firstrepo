import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled1/providers/counter_provider.dart';

class InkWellCustom extends StatelessWidget {
  Function function;
  Icon icon;
  String text;
  InkWellCustom(this.function, this.icon, this.text);
  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context);
    return Builder(
      builder: (ctx) =>
        InkWell(
        onTap: () {
          function();
          Scaffold.of(ctx).hideCurrentSnackBar();
          Scaffold.of(ctx).showSnackBar(
            SnackBar(
              duration: Duration(milliseconds: 500),
              content: Text('${text}${textProvider.Sum}(sum)'),
            ),
          );
        },
        child: icon,
      ),
    );
  }
}
