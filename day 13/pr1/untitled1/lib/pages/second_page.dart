import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled1/providers/counter_provider.dart';
class SecondPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context);
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          child: Icon(Icons.arrow_back),
          onTap: (() => Navigator.pushNamed(context, '/')),
        ),
      ),
      body: Container(
        //margin: EdgeInsets.only(top: 300, left: 40, right: 40),
        margin: EdgeInsets.all(25),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          InkWell(
            onTap: () => textProvider.changeDecrimentList(),
            child: Icon(Icons.remove),
          ),
          InkWell(
            onTap: () => textProvider.changeCounterList(),
            child: Icon(Icons.add),
          ),
        ]),
      ),
    );
  }
}

