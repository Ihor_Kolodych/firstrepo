import 'package:app_store/custom_container.dart';
import 'package:app_store/custom_hero.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CustomHero(
                textGameOfTheDay: 'GAME OF THE DAY',
                imageBackGround: 'assets/testImageBack.jpg',
                textNameGame: 'Wilmont\'s Warehouse',
                imageIcon: 'assets/testImageIcon.jpg',
                textGenre: 'Puzzle',
                price: 4.99,
              ),
            ),
          );
        },
        child: Padding(
          padding: const EdgeInsets.only(bottom: 200.0, left: 20, right: 20, top: 30),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                ),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,//.withOpacity(0.5),
                    blurRadius: 100,
                  ),
                ],
              ),
              child:
              CustomContainer(
                textGameOfTheDay: 'GAME OF THE DAY',
                imageBackGround: 'assets/testImageBack.jpg',
                textNameGame: 'Wilmont\'s Warehouse',
                imageIcon: 'assets/testImageIcon.jpg',
                textGenre: 'Puzzle',
                price: 4.99,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
