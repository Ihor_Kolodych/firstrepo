import 'package:flutter/material.dart';

class CustomTab extends StatelessWidget {
  final String text;

  const CustomTab(this.text);
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      fit: BoxFit.fill,
      child: Tab(
        text: text,
      ),
    );
  }
}
